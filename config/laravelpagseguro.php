<?php

return [
    'sandbox' => TRUE,//DEFINI SE SERÁ UTILIZADO O AMBIENTE DE TESTES
    
    'credentials' => [//SETA AS CREDENCIAIS DE SUA LOJA
        'email' => 'andreiapethres@me.com',
        'token' => '7327C696A9DA41348DC55817ED1E0AA2',
    ],
    'currency' => [//MOEDA QUE SERÁ UTILIZADA COMO MEIO DE PAGAMENTO
        'type' => 'BRL'
    ],
    'reference' => [//CRIAR UMA REFERENCIA PARA OS PRODUTOS VENDIDOS
        'idReference' => NULL
    ],
    'proxy' => [//CONFIGURAÇÃO PARA PROXY
        'user'     => NULL,
        'password' => NULL,
        'url'      => NULL,
        'port'     => NULL,
        'protocol' => NULL
    ],
    'url' => 'https://ws.sandbox.pagseguro.uol.com.br/v2/checkout',
];
