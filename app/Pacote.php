<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pacote extends Model
{
    protected $fillable = ['nome', 'descricao', 'produtos','valor','status','binario','porcentagem'];
}
