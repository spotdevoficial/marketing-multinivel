<?php

namespace App;

use Avatar;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Str;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'cpf', 'endereco', 'cidade', 'sexo', 'bairro',
        'estado', 'nascimento', 'telefone', 'pai_id', 'banco', 'agencia',
        'conta', 'operacao', 'admin', 'ativo', 'tipo_conta', 'pago', 'direcao', 'username', 'pacote', 'pacote_status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin()
    {
        return $this->admin; // this looks for an admin column in your users table
    }

    public function ativado()
    {
        return $this->ativo;
    }

    public function getShortName()
    {
        return strlen(Str::words($this->name, 1, '')) <= 5 ? Str::words($this->name, 2, '') : Str::words($this->name, 1, '');
    }

    public function getNascimento()
    {
        return date('d-m-Y', strtotime($this->nascimento));
    }

    public function memberSince()
    {
        return date('d F, Y', strtotime($this->created_at));
    }

    public function nIndicados()
    {
        return Referrals::where('pai_id', $this->id)->count();
    }

    public function nIndicadosDir()
    {
        $dir['esquerda'] = Referrals::where('pai_id', $this->id)->where('direcao', 'esquerda')->count();
        $dir['direita'] = Referrals::where('pai_id', $this->id)->where('direcao', 'direita')->count();

        return $dir;
    }

    public function getIndicados()
    {
        $reffer = Referrals::where('pai_id', Auth::user()->id)->get();

        $users = array();

        foreach ($reffer as $key => $r) {
            $users[$key] = $this->where('id', $r->user_id)->first();
        }

        return $users;
    }

    public function getFilhos($id = null, $count = false)
    {
        if (!$id) {
            $id = $this->id;
        }

        $reffer = Referrals::where('system_id', $id)->orderBy('direcao', 'ASC')->get();

        $users = array();

        foreach ($reffer as $key => $r) {
            $user = $this->where('id', $r->user_id)->first();
            $user->direcao = $this->getUserDirection($r->user_id);
            $users[$key] = $user;
        }

        if ($count) {
            return count($users);
        }

        return $users;
    }

    public function getUserDirection($user_id)
    {
        return Referrals::where('user_id', $user_id)->first()->direcao;
    }

    public function getIndicador($user_id)
    {
        $refer = Referrals::where('user_id', $user_id)->first();
        $user = $this->where('id', $refer->pai_id)->first();

        return $user->id . " - " . $user->username;

    }

    public function getAvatar()
    {
        return Avatar::create($this->name)->toBase64();
    }

    public function getSaldo()
    {
        return number_format($this->saldo, 2, ',', '.');
    }

    public function getUserPai()
    {
        $pai = $this->where('id', $this->pai_id)->first();
        return $pai ? $pai : new $this;
    }

    public function visitas()
    {
        return Visitas::where('user_id', $this->id)->distinct()->count();
    }

    public function cVouchers()
    {
        return Voucher::where('user_id', $this->id)->where('status', 0)->count();
    }

    public function display_children($parent = null, $level = 0)
    {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;
        $result = \DB::select("SELECT user_id FROM referrals WHERE system_id=$parent");

        foreach ($result as $row) {
            $count += 1 + $this->display_children($row->user_id, $level + 1);
        }
        return $count;
    }

    public function totalDireita($parent = null, $level = 0)
    {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='direita'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }
        foreach ($result as $row) {
            if ($level == 0) {
                if ($row->direcao == 'direita') {
                    $count += 1 + $this->totalDireita($row->user_id, $level + 1);
                } else {
                    $count += $this->totalDireita($row->user_id, $level + 1);
                }
            } else {
                $count += 1 + $this->totalDireita($row->user_id, $level + 1);
            }
        }
        return $count;
    }

    public function totalEsquerda($parent = null, $level = 0)
    {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='esquerda'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }

        foreach ($result as $row) {

            if ($row->direcao == 'esquerda') {
                $count += 1 + $this->totalEsquerda($row->user_id, $level + 1);
            } else {
                $count += $this->totalEsquerda($row->user_id, $level + 1);
            }
        }

        return $count;
    }

    public function ativaUser()
    {
        $this->ativo = 1;
        return $this->save();
    }

}
