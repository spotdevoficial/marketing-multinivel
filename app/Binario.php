<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Binario extends Model {

    public $ids = array();
    public $ids2 = array();
    protected $fillable = ['user_id', 'pontos', 'data'];

    public function getPontos($id) {
        $pontos = Binario::where('user_id', $id)->first()['pontos'];
        if ($pontos == '') {
            $pontos = 0;
        }
        return $pontos;
    }

    public function totalEsquerdaPt($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='esquerda'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }

        foreach ($result as $row) {

            if ($row->direcao == 'esquerda') {
                $count += 1 + $this->totalEsquerdaPt($row->user_id, $level + 1);
                $this->ids[$row->user_id] = $this->getPontos($row->user_id);
            } else {
                $count += $this->totalEsquerdaPt($row->user_id, $level + 1) . '<br>';
                $this->ids[$row->user_id] = $this->getPontos($row->user_id) . '<br>';
            }
        }

        return $count;
    }

    public function totalDireitaPts($parent = null, $level = 0) {
        if (!$parent) {
            $parent = $this->id;
        }
        $count = 0;

        if ($level == 0) {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent AND direcao ='direita'");
        } else {
            $result = \DB::select("SELECT user_id,direcao FROM referrals WHERE system_id=$parent");
        }
        foreach ($result as $row) {
            if ($level == 0) {
                if ($row->direcao == 'direita') {
                    $count += 1 + $this->totalDireitaPts($row->user_id, $level + 1);
                    $this->ids[$row->user_id] = $this->getPontos($row->user_id);
                } else {
                    $count += $this->totalDireitaPts($row->user_id, $level + 1);
                    $this->ids[$row->user_id] = $this->getPontos($row->user_id);
                }
            } else {
                $count += 1 + $this->totalDireitaPts($row->user_id, $level + 1);
                $this->ids[$row->user_id] = $this->getPontos($row->user_id);
            }
        }
        return $count;
    }

    public function totalDireita($parent = null) {
        $this->ids = array();
        $this->totalDireitaPts($parent);
        $u = User::where('id', $parent)->first();
        if ($u['less_points_dir'] == '') {
            $u['less_points_dir'] = 0;
        }
        if ($u['paid_points_dir'] == '') {
            $u['paid_points_dir'] = 0;
        }
        //- $u['paid_points_dir']) - $u['less_points_dir']
        $totalPontos = (array_sum($this->ids) - $u['paid_points_dir']) - $u['less_points_dir'];
        return $totalPontos;
    }

    public function totalEsquerda($parent = null) {
        $this->ids = array();
        $this->totalEsquerdaPt($parent);
        $u = User::where('id', $parent)->first();
        if ($u['less_points_esq'] == '') {
            $u['less_points_esq'] = 0;
        }
        if ($u['paid_points_esq'] == '') {
            $u['paid_points_esq'] = 0;
        }
        //- $u['paid_points_esq']) - $u['less_points_esq']
        $totalPontos = (array_sum($this->ids) - $u['paid_points_esq']) - $u['less_points_esq'];
        return $totalPontos;
    }

}
