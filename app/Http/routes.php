<?php
    use App\Binario;

//Route::resource('teste','PacoteController@index');
// FRONT ROUTES
Route::get('/', function () {
    return view('site.layout');
});

Route::post('/notificacao', 'Painel\PagamentoController@notificacao');
Route::get('/notificacao', 'Painel\PagamentoController@notificacao');

Route::post('/binario6737e67ewwqqetw', 'BinarioController@index');
Route::get('/binario6737e67ewwqqetw', 'BinarioController@index');

Route::get('/sala', function(){
    \App\VisitasSala::create(['ip' => Request::ip()]);
    return redirect('http://www.gvolive.com/conference,sevenoficial');
});

// USER PANEL ROUTES
Route::get('/painel', function () {
    return redirect('/painel/home');
});

Route::get('/cadastro', function () {
    return redirect('/');
});

Route::group(['middleware' => 'web'], function () {
    Route::get('/cadastro/{indicacao}', 'CadastroController@index');
    Route::post('/cadastro/{indicacao}', 'CadastroController@store');
});

Route::group(['middleware' => ['web', 'auth.user'], 'prefix' => 'painel'], function () {

    Route::get('inativo', 'Painel\GNetController@index');
    Route::get('inativo/boleto', 'Painel\GNetController@gerarBoleto');
    Route::post('inativo/cartao', 'Painel\GNetController@pagarCartao');
    Route::get('inativo/testes', 'Painel\GNetController@testes');

    Route::post('ativar-conta', 'Painel\UserController@ativarConta');

    Route::get('meus-dados', 'Painel\UserController@index');
    Route::post('meus-dados', 'Painel\UserController@update');
});

Route::group(['middleware' => ['web', 'auth.user', 'ativo'], 'prefix' => 'painel'], function () {
    Route::get('home', function () {
        $binario = new Binario();
        $binario['esquerda'] = $binario->totalEsquerda(\Auth::user()->id);
        $binario['direita'] = $binario->totalDireita(\Auth::user()->id);
        $pacoteCurr = App\Pacote::where('id', \Auth::user()->pacote)->first();
        return view('painel.pages.home', compact('pacoteCurr','binario'));
    });

    Route::get('upgrade', 'Painel\UpgradeController@index');
    Route::post('upgrade', 'Painel\UpgradeController@index');

    Route::get('minha-rede', 'Painel\RedeController@index');
    Route::post('minha-rede/busca', 'Painel\UserController@buscar');

    Route::get('minha-rede/{id}', 'Painel\RedeController@interna');
    Route::post('minha-rede/update/{id}', 'Painel\RedeController@update');


    // REDE TESTE
    Route::get('teste/minha-rede', 'Painel\RedeTesteController@index');
    Route::post('teste/minha-rede/busca', 'Painel\UserController@buscar');

    Route::get('teste/minha-rede/{id}', 'Painel\RedeTesteController@interna');
    Route::post('teste/minha-rede/update/{id}', 'Painel\RedeTesteController@update');


    Route::post('suporte/ajax', 'Painel\SuporteController@storeAjax');

    Route::get('vouchers', function () {
        return view('painel.pages.vouchers');
    });

    Route::get('meus-indicados', function () {
        return view('painel.pages.directs');
    });

    Route::resource('suporte', 'Painel\SuporteController');
    Route::resource('materiais', 'Painel\MateriaisController');

    Route::get('transacoes', 'Painel\ExtratosController@index');
    Route::post('transacoes', 'Painel\ExtratosController@index');

    Route::get('produtos', 'Painel\ProdutosController@index');
    Route::get('produtos/add', 'Painel\ProdutosController@create');
    Route::post('produtos/add', 'Painel\ProdutosController@createPost');

    Route::get('meu-carrinho', 'Painel\MeuCarrinhoController@index');
    Route::get('meu-carrinho/endereco/', 'Painel\EnderecoController@index');

    Route::get('todos-pedidos', 'Painel\MeusPedidosController@todos');

    Route::get('meus-pedidos', 'Painel\MeusPedidosController@index');
    Route::get('meus-pedidos/pedido/{pedido}', 'Painel\MeusPedidosController@pedido');
    Route::get('meus-pedidos/add/{endereco}/{codigo}', 'Painel\MeusPedidosController@add');

    Route::post('meu-carrinho/endereco/add', 'Painel\EnderecoController@add');

    Route::get('meu-carrinho/add/{product_id}/', 'Painel\MeuCarrinhoController@add');
    Route::get('meu-carrinho/remove/{product_id}/{max?}/', 'Painel\MeuCarrinhoController@remove');
    Route::get('meu-carrinho/qtd/{product_id}/{max?}/', 'Painel\MeuCarrinhoController@qtd');

    Route::get('cadastro', 'Painel\CadastroInternoController@index');
    Route::post('cadastro', 'Painel\CadastroInternoController@store');

    // Usuario
    Route::get('muda_lado', 'Painel\UserController@muda_lado');
    Route::post('muda_lado', 'Painel\UserController@muda_lado');
});

Route::group(['middleware' => ['web'], 'prefix' => 'painel'], function () {

    // Authentication Routes...
    Route::get('login', 'Auth\AuthController@showLoginUser');
    Route::post('login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetFormUser');
    Route::post('password/email', 'Auth\PasswordController@postEmailUser');
    Route::post('password/reset', 'Auth\PasswordController@reset');
});

// ADMIN PANEL ROUTES
Route::get('/admin', function () {
    return redirect('/admin/home');
});

Route::group(['middleware' => ['web'], 'prefix' => 'admin'], function () {
    // Authentication Routes...
    Route::get('login', 'Auth\AuthController@showLoginForm');
    Route::post('login', 'Auth\AuthController@login');
    Route::get('logout', 'Auth\AuthController@logout');

    // Password Reset Routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\PasswordController@reset');
});

Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin'], function () {

    Route::get('home', function () {
        return view('admin.pages.home');
    });

    // Materiais
    Route::resource('materiais', 'Admin\MateriaisController');

    // Usuario
    Route::get('meus-dados', 'Painel\UserController@index');
    Route::post('meus-dados', 'Painel\UserController@update');

    // Vouchers
    Route::get('vouchers', 'Admin\VoucherController@index');
    Route::get('vouchers/adicionar/{user}', 'Admin\VoucherController@create');
    Route::post('vouchers/salvar/{user}', 'Admin\VoucherController@store');
    Route::post('vouchers/remover/{user}', 'Admin\VoucherController@destroy');
    Route::get('vouchers/remover/{user}', 'Admin\VoucherController@remover');




// Materiais
    Route::resource('pacote', 'Admin\PacoteController');
});
