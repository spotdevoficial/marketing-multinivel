<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Função para calcular o frete
    function calculaFrete($cod_servico, $cep_destino, $peso,
        $altura='2', $largura='11', $comprimento='16',
        $valor_declarado='0.50') {

         # Código dos Serviços dos Correios
         # 41106 PAC sem contrato
         # 40010 SEDEX sem contrato
         # 40045 SEDEX a Cobrar, sem contrato
         # 40215 SEDEX 10, sem contrato

         $correios =
            "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx".
            "?nCdEmpresa=&sDsSenha=&sCepOrigem=36570000".
            "&sCepDestino=".$cep_destino."&nVlPeso=".$peso.
            "&nCdFormato=1&nVlComprimento=".$comprimento.
            "&nVlAltura=".$altura."&nVlLargura=".$largura.
            "&sCdMaoPropria=n&nVlValorDeclarado=".$valor_declarado.
            "&sCdAvisoRecebimento=n&nCdServico=".$cod_servico.
            "&nVlDiametro=0&StrRetorno=xml";

         $xml = simplexml_load_file($correios);
         if($xml->cServico->Erro == '0')
         {
            
            return $xml->cServico->Valor;
            }
         else
         {
            return false;
            }

    }

}
