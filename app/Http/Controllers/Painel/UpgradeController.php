<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pacote;
use App\User;
use PagSeguroConfig;
use PagSeguroConfigWrapper;
use PagSeguroPaymentRequest;
use PagSeguroShippingType;
use App\Pagamentos;

class UpgradeController extends Controller {

    public function index() {
        $pacoteAtual = Pacote::where('id', \Auth::user()->pacote)->first();
        $pacotes = Pacote::where('status', 1)->where('valor', '>', $pacoteAtual['valor'])->orderBy('valor', 'ASC')->get();
        $desconto = $pacoteAtual['valor'];
        if (@$_GET['novoUpgrade'] == 1 && is_numeric(@$_GET['id'])) {
            $pacoteId = $_GET['id'];
            $pacotes = Pacote::where('id', $pacoteId)->first();
            $pacotes['valor'] = floatval($pacotes['valor'] - $desconto);
            //pagseguro
            $paymentRequest = new PagSeguroPaymentRequest();
            $paymentRequest->addItem($pacotes['id'], $pacotes['nome'], 1, $pacotes['valor']);

            $paymentRequest->setSender(
                    \Auth::user()->name, \Auth::user()->email, '', '', \Auth::user()->cpf, '');

            $paymentRequest->setCurrency("BRL");
            $reference = md5(\Auth::user()->name . $pacotes['id'] . time());
            $paymentRequest->setReference($reference);
            $paymentRequest->setRedirectUrl(env('PAGSEGURO_REDIRECT'));

// URL para onde serão enviadas notificações (POST) indicando alterações no status da transação  
            $paymentRequest->addParameter('notificationURL', env('PAGSEGURO_NOTIFICATION'));
            try {

                $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()  
                $checkoutUrl = $paymentRequest->register($credentials);
                Pagamentos::create(['user_id' => \Auth::user()->id, 'reference' => $reference, 'status' => 1, 'paymentMethod' => $checkoutUrl, 'paymentLink', 'pacote' => $pacoteId]);
                echo $checkoutUrl;
            } catch (PagSeguroServiceException $e) {
                die($e->getMessage());
            }
        } else {
           return view('painel.pages.upgrade', compact('pacotes', 'desconto', 'pacoteAtual'));
        }
    }

}
