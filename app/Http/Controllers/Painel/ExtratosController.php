<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\extratos;

class ExtratosController extends Controller {

    public function index() {
        $Extratos=extratos::where('beneficiado',\Auth::user()->id)->get();
    
        return view('painel.pages.extratos',compact('Extratos'));
    }

}
