<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Carrinho;
use App\Produtos;
use App\Pedidos;

use PagSeguroConfig;
use PagSeguroConfigWrapper;
use PagSeguroPaymentRequest;
use PagSeguroShippingType;
use App\Enderecos;

class MeusPedidosController extends Controller {

    public function index() {

        $pedidos = Pedidos::where('user_id', \Auth::user()->id)->get()->toArray();


        foreach ($pedidos as $key=>$pedido) {
            $produto = Produtos::where('id', Carrinho::where('pedido',$pedido['id'])->first()['product_id'])->first();
            $pedidos[$key]['img'] = $produto['img'];
        }

        return view('painel.pages.meus-pedidos',compact('pedidos'));

    }
    public function todos() {

        $pedidos = Pedidos::where('status', 'Pago')->get()->toArray();


        foreach ($pedidos as $key=>$pedido) {
            $produto = Produtos::where('id', Carrinho::where('pedido',$pedido['id'])->first()['product_id'])->first();
            $pedidos[$key]['img'] = $produto['img'];
        }

        return view('painel.pages.todos-pedidos',compact('pedidos'));

    }

    public function add($endereco, $codigo){

        $endereco = Enderecos::where('id',$endereco)->first();
        $produtos = Carrinho::carrinhoAtual();

        if(!$produtos )
            return redirect("/painel/meus-pedidos");

        // Calcula o subtotal
        $subtotal = 0;
        $pesoTotal = 0;
        foreach ($produtos as $produto) {

            $quantidade = $produto['quantidade'];
            $preco = $produto['produto']['preco']*$quantidade;
            $subtotal += $preco;
            $pesoTotal += $quantidade*$produto['produto']['peso'];

        }




        // Calcula o frete do pedido
        $frete = $this->calculaFrete($codigo, $endereco['cep'],$pesoTotal);
        $total = $subtotal+$frete;

        // Cria um novo pedido
        $pedido = new Pedidos;
        $pag = $this->pagSeguro($total);
        $pedido->id_pag =  $pag['id_pag'];
        $pedido->user_id = \Auth::user()->id;
        $pedido->id_endereco = $endereco['id'];
        $pedido->status = "Aguardando pagamento";
        $pedido->preco = $total;
        $pedido->date =  date("Y-m-d");


        $pedido->save();

         $carrinhos = Carrinho::where('pedido',0)->where(
        'user_id',\Auth::user()->id)->update(['pedido' =>  $pedido->id]);

        return redirect('/painel/meus-pedidos/pedido/'.$pedido->id_pag);

    }



    public function pagSeguro($total) {
        $id = mt_rand(1262055681,1262055681);

         $paymentRequest = new PagSeguroPaymentRequest();

         $paymentRequest->addItem(mt_rand(1262055681,1262055681), "#Seu Pedido", 1, number_format($total,2));
         $paymentRequest->setSender(
                    \Auth::user()->name, \Auth::user()->email, '', '', \Auth::user()->cpf, '');

         $paymentRequest->setCurrency("BRL");
            $reference = md5(\Auth::user()->name . $id . time());
            $paymentRequest->setReference($reference);
            $paymentRequest->setRedirectUrl(env('PAGSEGURO_REDIRECT'));


            $paymentRequest->addParameter('notificationURL', env('PAGSEGURO_NOTIFICATION'));

                $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()

                $checkoutUrl = $paymentRequest->register($credentials);


                return array('id_pag'=> mt_rand(1262055681,1262055681), 'redirect'=> $checkoutUrl );


    }

    // Retorna detalhes de um pedido
    public function pedido($id){

        $pedido = Pedidos::where('id', $id)->first();

        // Verifica se o pedido é do usuário mesmo
        if($pedido['user_id'] != \Auth::user()->id)
            return redirect("/painel/meus-pedidos/");

        // Pega o endereco e os produtos do pedido
        $endereco = Enderecos::where('id',$pedido['id_endereco'])->first();
        $produtos = Carrinho::carrinhoPedido($id);

        return view('painel.pages.pedido',compact('produtos','endereco','pedido'));

    }

}
