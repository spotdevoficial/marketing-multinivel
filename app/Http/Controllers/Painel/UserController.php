<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\Voucher;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('painel.pages.profile');
    }

    public function ativarConta()
    {
        $post = \Input::all();

        $atulizaVoucher = Voucher::where('voucher', $post['ativarVoucher'])
            //->where('user_id', \Auth::user()->pai_id)
            ->where('status', 0)
            ->update(['status' => 1, 'activated_id' => \Auth::user()->id]);

        if ($atulizaVoucher) {

            if (\Auth::user()->update(['ativo' => 1])) {
                return redirect('/painel/home')
                    ->with('success', 'Parabéns! Sua conta foi ativada com sucesso!');
            } else {
                return redirect('/painel/home')
                    ->withErrors(['Não foi possivel ativar sua conta. Por favor entre em contato com o Suporte.']);
            }
        } else {
            return redirect('/painel/home')
                ->withErrors(['Voucher não encontrado. Ou já foi utilizado.']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data = \Input::all();

        $valida = [
            'name' => 'required|max:255',
            'cpf' => 'cpf',
            'endereco' => 'required',
            'bairro' => 'required',
            'sexo' => 'required',
            'cidade' => 'required',
            'estado' => 'required',
            'nascimento' => 'date',
        ];

        if (!empty($data['password'])) {
            $valida = array_merge($valida, ['password' => 'required|confirmed|min:6',]);
        }

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            return redirect('/painel/meus-dados')
                ->withErrors($validator)
                ->withInput();
        }

        $dados = [
            'name' => $data['name'],
            'cpf' => $data['cpf'],
            'endereco' => $data['endereco'],
            'bairro' => $data['bairro'],
            'cidade' => $data['cidade'],
            'sexo' => $data['sexo'],
            'estado' => $data['estado'],
            'nascimento' => date('Y-m-d', strtotime($data['nascimento'])),
            'telefone' => $data['telefone'],
            'banco' => $data['banco'],
            'agencia' => $data['agencia'],
            'conta' => $data['conta'],
            'tipo_conta' => $data['tipo_conta'],
            'operacao' => $data['operacao'],
            'direcao' => $data['direcao'],
        ];

        if (!empty($data['password'])) {
            $dados = array_merge($dados, ['password' => bcrypt($data['password']),]);
        }

        $user = \Auth::user()->update($dados);

        if ($user) {
            return redirect('/painel/meus-dados')
                ->with('success', 'Perfil Atualizado');
        } else {
            return redirect('/painel/meus-dados')
                ->withErrors(['Não foi possivel atualizar, tente novamente.']);
        }
    }

    public function buscar(Request $request)
    {
        $usuario = User::where('id', $request->busca)->orWhere('username', $request->busca)->first();
        if ($usuario) {
            return '<meta http-equiv="refresh" content="0;URL=' . url('painel/minha-rede/' . $usuario->id) . '">';
        } else {
            return "<div class='alert alert-danger'>Sem resultados</div>";
        }
    }

    public function muda_lado(Request $request)
    {
        $data = \Input::all();
        if ($data['lado'] == 'esquerda' or $data['lado'] == 'direita') {
            \Auth::user()->update(['direcao' => $data['lado']]);
            echo 'ok';
        } else {
            echo 'fail';
        }
    }

}
