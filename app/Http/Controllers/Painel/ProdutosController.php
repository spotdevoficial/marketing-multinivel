<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Produtos;
use Model;
use Validator;
use App\Pedidos;

class ProdutosController extends Controller {

    public function index() {
        $busca = \Input::get('busca', null);

        if(empty($busca)){
            $produtos=Produtos::get();
        }else{
            $produtos=Produtos::where('nome','like',"%$busca%")->get();
        }
        return view('painel.pages.produtos',compact('produtos','busca'));
    }

    public function create() {

        return view('painel.pages.produtosAdd');
    }
    public function createPost(Request $dados) {


    	$validator = Validator::make($dados->all(), [
            'nome' => 'required|max:255',
            'preco' => 'required|numeric',
            'peso' => 'required|numeric',
            'img' => 'image',
            'categoria' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return redirect('painel/produtos/add')
                        ->withErrors($validator)
                        ->withInput();
        }


        // Foi
        $int= mt_rand(1262055681,1262055681);


		$fileName = date("YmdHis",$int).".".$dados->file('img')->getClientOriginalExtension();;
        

        $dados->file('img')->move('uploads', $fileName);

    	$dados = $dados->all();
    	$dados['img'] = 'uploads/'.$fileName;

    	$produto = Produtos::create($dados);
       return redirect()->back();
    }

}
