<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\GNetRepository;
use App\User;
use App\Pacote;
use PagSeguroConfig;
use PagSeguroConfigWrapper;
use PagSeguroPaymentRequest;
use PagSeguroShippingType;
use App\Pagamentos;

class GNetController extends Controller {

    private $repository;

    public function __construct(GNetRepository $repository) {
        $this->repository = $repository;
    }

    public function index() {

        if (@$_GET['novopagamento'] == 1) {
            $pacoteId = \Auth::user()->pacote;
            $pacotes = Pacote::where('id', $pacoteId)->first();

            //pagseguro
            $paymentRequest = new PagSeguroPaymentRequest();
            $paymentRequest->addItem($pacotes['id'], $pacotes['nome'], 1, $pacotes['valor']);

            $paymentRequest->setSender(
                    \Auth::user()->name, \Auth::user()->email, '', '', \Auth::user()->cpf, '');

            $paymentRequest->setCurrency("BRL");
            $reference = md5(\Auth::user()->name . $pacotes['id'] . time());
            $paymentRequest->setReference($reference);
            $paymentRequest->setRedirectUrl(env('PAGSEGURO_REDIRECT'));

// URL para onde serão enviadas notificações (POST) indicando alterações no status da transação  
            $paymentRequest->addParameter('notificationURL', env('PAGSEGURO_NOTIFICATION'));
            try {

                $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()  
                $checkoutUrl = $paymentRequest->register($credentials);
                Pagamentos::create(['user_id' => \Auth::user()->id, 'reference' => $reference, 'status' => 1, 'paymentMethod' => $checkoutUrl, 'paymentLink', 'pacote' => $pacoteId]);
                echo $checkoutUrl;
            } catch (PagSeguroServiceException $e) {
                die($e->getMessage());
            }
        } else {
            if (\Auth::user()->ativo) {
                return redirect('/painel');
            } else {
                return view('painel.pages.inativo_gnet');
            }
        }
    }

    public function hue() {
        echo 'hue';
    }

    public function gerarBoleto() {
        $trans = $this->repository->createTransaction();
        $boleto = $this->repository->createBoleto($trans['data']['charge_id']);
        return redirect($boleto['data']['link']);
    }

    public function pagarCartao() {
        $trans = $this->repository->createTransaction();
        $cartao = $this->repository->createCard($trans['data']['charge_id'], \Input::get('payment_token'));
        if (isset($cartao['data']['charge_id'])) {
            return 'Pagamento efetuado, em breve sua conta será ativada.';
        } else {
            return $cartao;
        }
    }

    public function notificacao() {
        header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
        $trans = $this->pagSeguroRepository->notification(\Input::get('notificationCode'));
        return $this->pagSeguroRepository->updateTransaction($trans);
        //var_dump($_POST);
    }

}
