<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Referrals;
use App\User;
use Illuminate\Http\Request;
use App\Binario;

class RedeController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $binario = new Binario();
        $binario['esquerda'] = $binario->totalEsquerda(\Auth::user()->id);
        $binario['direita'] = $binario->totalDireita(\Auth::user()->id);

        return view('painel.pages.rede',compact('binario'));
    }

    public function interna($id) {
        $user_interna = User::where('id', $id)->first();
        if ($user_interna) {
            return view('painel.pages.rede_interna', compact('user_interna'));
        } else {
            return redirect('/painel/minha-rede')
                            ->withErrors(['Usuario não encontrado']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Referrals::updateDirection($id, \Input::get('direcao'))) {
            return "<div class='alert alert-success'>Usuario ID: <b>$id</b> Atualizado!</div>";
        } else {
            return "<div class='alert alert-danger'>Não foi possivel atualizar o Usuario: <b>$id</b></div>";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
