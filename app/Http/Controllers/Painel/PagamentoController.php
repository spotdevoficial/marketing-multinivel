<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\PagSeguroRepository;
use Illuminate\Http\Request;
use App\User;
use App\Pedidos;
use Builder;

class PagamentoController extends Controller {

    private $pagSeguroRepository;

    public function __construct(PagSeguroRepository $pagSeguroRepository) {
        $this->pagSeguroRepository = $pagSeguroRepository;
    }

    public function index() {
        if (\Auth::user()->ativo) {
            return redirect('/painel');
        } else {
            //$idSessao = $this->pagSeguroRepository->getIdSessao();
            //return view('painel.pages.inativo', compact('idSessao'));
            return view('painel.pages.inativo');
        }
    }

    public function checkout() {
        $trans = $this->pagSeguroRepository->checkout();
        $url = 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $trans->code;
        var_dump($url);
        exit;
        return redirect($url);
    }

    public function pagarCartao() {
        $trans = $this->pagSeguroRepository->transCartao();
        if ($this->pagSeguroRepository->saveTransaction($trans)) {
            return redirect('/painel');
        } else {
            return redirect('/painel');
        }
    }

    public function geraBoleto() {
        $trans = $this->pagSeguroRepository->transBoleto();
        if ($this->pagSeguroRepository->saveTransaction($trans)) {
            return redirect($trans->paymentLink);
        } else {
            return redirect('/painel');
        }
    }

    public function confirmacao() {
        var_dump(\Input::get('transaction_id'));
    }

    public function testes() {
        var_dump($this->pagSeguroRepository->notification(\Input::get('code')));
    }

    public function notificacao() {

        
        
        header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
        //var_dump($_POST);
        $email = env('PAGSEGURO_EMAIL');
        $token = env('PAGSEGURO_TOKEN_SANDBOX');
        $noti = $_POST['notificationCode'];


         $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/' . $noti . '?email=' . $email . '&token=' . $token;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $transactionB = curl_exec($curl);
        curl_close($curl);

        $transaction = simplexml_load_string($transactionB);
        $xml   = simplexml_load_string($transactionB, 'SimpleXMLElement', LIBXML_NOCDATA);

        $array = json_decode(json_encode((array)$xml), TRUE);

    

       if(@substr($array['items']['item']['description'], 0, 3) == "Seu") { 
        $id = $array['items']['item']['id'];
      
        $pedidos = Pedidos::where('id_pag', $id)->first();
        $pedidos->status = "Pago";
        $pedidos->save();

        return "Pago com sucesso.";


         }else { 
         

         $this->pagSeguroRepository->updateTransaction($transaction);
         
         }




    }

}
