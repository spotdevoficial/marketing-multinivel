<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Pacote;
use App\Referrals;
use App\User;
use App\Visitas;
use Illuminate\Http\Request;
use Validator;

class CadastroController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($indicacao, Request $request)
    {
        $indicador = User::where('username', $indicacao)->orWhere('email', $indicacao)->first();
        $pacotes = Pacote::all();

        if ($indicador && $indicador->ativo == 1) {

            Visitas::create(['ip' => $request->ip(), 'user_id' => $indicador->id]);

            return view('painel.auth.register', compact('indicador', 'pacotes'));
        } else {
            abort(404);
            return false;
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $indicacao)
    {
        $indicador = User::where('username', $indicacao)->first();
        $reffer = new Referrals();
        $systemId = $reffer->searchSystemId($indicador->id, $indicador->direcao);

        if ($indicador) {

            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users',
                'username' => 'required|max:20|unique:users',
                'password' => 'required|confirmed|min:6',
                'pacote' => 'required|integer',
                'cpf' => 'cpf',
                'endereco' => 'required',
                'bairro' => 'required',
                'cidade' => 'required',
                'estado' => 'required',
                'telefone' => 'phone',
                'nascimento' => 'date',
            ]);

            if ($validator->fails()) {
                return redirect('/cadastro/' . $indicacao)
                    ->withErrors($validator)
                    ->withInput();
            }

            $data = \Input::all();

            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'username' => $data['username'],
                'password' => bcrypt($data['password']),
                'cpf' => $data['cpf'],
                'endereco' => $data['endereco'],
                'bairro' => $data['bairro'],
                'cidade' => $data['cidade'],
                'sexo' => $data['sexo'],
                'estado' => $data['estado'],
                'nascimento' => date('Y-m-d', strtotime($data['nascimento'])),
                'telefone' => $data['telefone'],
                'pai_id' => $indicador->id,
                'direcao' => $indicador->direcao,
                'pacote' => $data['pacote']
            ]);

            if ($user) {
                $reffer->user_id = $user->id;
                $reffer->pai_id = $indicador->id;
                $reffer->system_id = $systemId['pai_id'];
                $reffer->direcao = $systemId['direcao'] ? $systemId['direcao'] : $indicador->direcao;
                $reffer->save();
            }

            if (\Auth::login($user)) {
                return redirect('/painel');
            } else {
                return redirect('/painel');
            }
        } else {
            abort(404);

            return false;
        }
    }

}
