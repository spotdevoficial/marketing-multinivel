<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Binario;
use App\User;
use App\extratos;
use App\Pacote;

class BinarioController extends Controller {

    public $hue;

    public function index() {
        $Bin = new Binario();
        //$Bin->totalEsquerda(1);
        $today = date("d-m-Y");
        $usuarios = User::where('ativo', 1)->get();
        foreach ($usuarios as $value) {
            $BinarioEsq = $Bin->totalEsquerda($value['id']);
            $BinarioDireita = $Bin->totalDireita($value['id']);
            $pacote = Pacote::where('id', $value['pacote'])->first();


            if ($BinarioEsq > $BinarioDireita and $pacote['binario'] == 1) {
                if ($BinarioDireita !== 0) {
                    $novoSaldo = $value['saldo'] + $BinarioDireita;
                    extratos::create(['user_id' => '', 'data' => $today, 'descricao' => 'Pagamento de binário', 'valor' => $BinarioDireita, 'beneficiado' => $value['id']]);
                    extratos::create(['user_id' => '', 'data' => $today, 'descricao' => 'Pagar Binário', 'valor' => $BinarioDireita, 'beneficiado' => 1]);

                    User::where('id', $value['id'])->update(['saldo' => $novoSaldo]);
                    $lessPoints = $BinarioDireita + $value['less_points_esq'];
                    User::where('id', $value['id'])->update(['less_points_esq' => $lessPoints]);
                    User::where('id', $value['id'])->update(['less_points_dir' => 0]);
                    $points = $value['paid_points_dir'] + $BinarioDireita;
                    User::where('id', $value['id'])->update(['paid_points_dir' => $points]);
                }
            } else {
                if ($BinarioEsq !== 0 and $pacote['binario'] == 1) {
                    $novoSaldo = $value['saldo'] + $BinarioEsq;
                    extratos::create(['user_id' => '', 'data' => $today, 'descricao' => 'Pagamento de binário', 'valor' => $BinarioEsq, 'beneficiado' => $value['id']]);
                    extratos::create(['user_id' => '', 'data' => $today, 'descricao' => 'Pagar Binário', 'valor' => $BinarioEsq, 'beneficiado' => 1]);
                    User::where('id', $value['id'])->update(['saldo' => $novoSaldo]);
                    User::where('id', $value['id'])->update(['saldo' => $novoSaldo]);
                    $lessPoints = $BinarioEsq + $value['less_points_dir'];
                    User::where('id', $value['id'])->update(['less_points_dir' => $lessPoints]);
                    //User::where('id', $value['id'])->update(['less_points_esq' => 0]);
                    $points = $value['paid_points_esq'] + $BinarioEsq;
                    User::where('id', $value['id'])->update(['paid_points_esq' => $points]);
                }
            }
        }
    }

}
