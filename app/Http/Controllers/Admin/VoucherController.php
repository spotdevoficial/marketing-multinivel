<?php

namespace App\Http\Controllers\Admin;

use App\Voucher;
use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();
        return view('admin.pages.vouchers', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user)
    {
        $usuario = User::find($user);
        return view('admin.pages.addvoucher', compact('usuario'));
    }

    public function remover($user)
    {
        $usuario = User::find($user);
        return view('admin.pages.removevoucher', compact('usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, \Faker\Generator $faker)
    {
        $data = \Input::all();

        $data['user_id'] = $id;

        for ($i = 0; $i < $data['quantidade']; $i++) {
            $data['voucher'] = $faker->sha1;
            $create = Voucher::create($data);
        }

        if ($create) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers Gerados Com Successo!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $data = \Input::all();
        for ($i = 0; $i < $data['quantidade']; $i++) {
            $voucher = Voucher::where('user_id', $user)->first();
            if ($voucher) {
                $destroy = Voucher::destroy($voucher->id);
            } else {
                $destroy = true;
                break;
            }
        }

        if ($destroy) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers removidos com sucesso!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;

        }
    }
}
