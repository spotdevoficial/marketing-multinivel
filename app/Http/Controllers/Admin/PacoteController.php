<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pacote;

class PacoteController extends Controller {

    public function index() {
        $pacotes = Pacote::all();
        return view('admin.pages.pacote', compact('pacotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view('admin.pages.pacotes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $pacote = new Pacote();
        $pacote->nome = $request->nome;
        $pacote->descricao = $request->descricao;
        $pacote->produtos = $request->produtos;
        
        $pacote->valor = $request->valor;
        $pacote->status = $request->status;
        if ($pacote->save()) {
            return <<<EOL
                 <div class="alert alert-success fade in">
                     Pacote Adicionado
                 </div>
EOL;
        } else {
            return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        //
    }

}
