<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    //

    protected $fillable = [
        'voucher', 'user_id', 'status', 'activated_id',
    ];

    public function getUserNameActivated()
    {
        $user = User::where('id', $this->activated_id)->first();
        return $this->activated_id ? $user->name : null;
    }
}
