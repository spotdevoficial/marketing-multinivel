<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referrals extends Model
{

    protected $fillable = ['user_id', 'system_id', 'pai_id', 'direcao'];

    public function searchSystemId($indicador, $direcao = 'esquerda')
    {

        $procura = true;
        $system_id = $indicador;
        while ($procura) {

            if (isset($proximoFilhoId)) {
                $system_id = $proximoFilhoId;
            }

            $filho = $this->where([['system_id', $system_id], ['direcao', $direcao]])->first();

            if ($filho) {

                if ($this->where([['system_id', $filho->user_id], ['direcao', $direcao]])->first()) {
                    $proximoFilhoId = $this->where([['system_id', $filho->user_id], ['direcao', $direcao]])->first()->user_id;
                    $procura = true;
                } else {
                    $system_id = $filho->user_id;
                    $procura = false;
                }
            }else{
                $procura = false;
            }

        }

        return ['pai_id' => $system_id, 'direcao' => $direcao];

    }

    public static function updateDirection($id, $direcao){
        $refer = new Referrals();
        return $refer->where('user_id', $id)->update(['direcao' => $direcao]);
    }

}
