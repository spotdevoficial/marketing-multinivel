<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class extratos extends Model {

    protected $fillable = ['user_id', 'data', 'descricao', 'valor', 'beneficiado'];

    public function userName($id) {
        $dat = User::where('id', $id)->first();
        return strlen(Str::words($dat['name'], 1, '')) <= 5 ? Str::words($dat['name'], 2, '') : Str::words($dat['name'], 1, '');
    }

}
