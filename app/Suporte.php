<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suporte extends Model
{

    protected $table = 'suporte';

    protected $fillable = [
        'user_id', 'assunto', 'body',
    ];
}
