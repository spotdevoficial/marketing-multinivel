<!-- Main Header -->
<header class="main-header">
    <?php
    global $request;
    $uriPainel = $request->segment(1);
    ?>

    <a href="{{ url('/'.$uriPainel.'/home') }}" class="logo" style="background: #FFFFFF;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"> <img src="{{url('/img/logo-escritorio.png')}}"/></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="{{url('/img/logo-escritorio.png')}}"/></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <li><a href="{{ url('/'.$uriPainel.'/login') }}">Login</a></li>
                    @else
                            <!-- User Account Menu -->
                    <li class="dropdown user user-menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                            <img src="{{Auth::user()->getAvatar()}}" class="user-image" alt="User Image"/>
                            <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
                                <img src="{{Auth::user()->getAvatar()}}" class="img-circle" alt="User Image"/>
                                <p>
                                    {{ Auth::user()->name }}
                                    <small>Membro desde {{Auth::user()->memberSince()}}</small>
                                </p>
                            </li>

                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ url('/'.$uriPainel.'/meus-dados') }}" class="btn btn-default btn-flat">Meus Dados</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/'.$uriPainel.'/logout') }}" class="btn btn-default btn-flat">Encerrar sessão</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    @endif

                            <!-- Control Sidebar Toggle Button -->
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
            </ul>
        </div>
    </nav>
</header>