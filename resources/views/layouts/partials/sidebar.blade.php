<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{Auth::user()->getAvatar()}}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        @endif
        <?php
        global $request;
        $uriPainel = $request->segment(1);
        $uriPage = $request->segment(2);
        ?>
        @if(Auth::user()->ativo)
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" disabled placeholder="Pesquisar..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU PRINCIPAL</li>
            <!-- Optionally, you can add icons to the links -->

            <li class="{{$uriPage == 'home' ? 'active' : ''}}"><a href="{{ url('/'.$uriPainel.'/home') }}"><i class='fa fa-dashboard'></i> <span>Dashboard</span></a></li>

            @if (! Auth::guest()  && $uriPainel == 'painel')
                        <li class="{{$uriPage == 'meus-dados' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-dados')}}"><i class='fa fa-user'></i> <span>Meus Dados</span></a></li>
            <li class="{{$uriPage == 'cadastro' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/cadastro')}}"><i class='fa fa-support'></i> <span>Cadastro</span></a></li>

            <li class="{{$uriPage == 'upgrade' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/upgrade')}}"><i class='fa fa-trophy'></i> <span>Upgrade</span></a></li>
            <li class="{{$uriPage == 'transacoes' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/transacoes')}}"><i class='fa fa-random'></i> <span>Extrato financeiro</span></a></li>

            <!-- Botão deslizador da Loja virtual -->
            <li class="treeview">
                <a href="#">
                  <i class="fa fa-shopping-cart "></i> <span>Loja Virtual</span>
                  <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li class="{{$uriPage == 'produtos/add' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos/add')}}"><i class="fa fa-circle-o"></i> Adicionar Produtos</a></li>
                  <li class="{{$uriPage == 'produtos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos')}}"><i class="fa fa-circle-o"></i> Produtos</a></li>
                  <li class="{{$uriPage == 'meu-carrinho' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meu-carrinho')}}"><i class="fa fa-circle-o"></i> Meu carrinho</a></li>
                </ul>
            </li>
            <!-- Fim do botão deslizador da Loja virtual -->

             <li class="{{$uriPage == 'todos-pedidos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/todos-pedidos')}}"><i class='fa fa-truck '></i> <span>Todos pedidos</span></a></li>
            <li class="{{$uriPage == 'meus-pedidos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-pedidos')}}"><i class='fa fa-truck '></i> <span>Meus pedidos</span></a></li>
            <li class="{{$uriPage == 'meus-indicados' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-indicados')}}"><i class='fa fa-exchange'></i> <span>Indicados Diretos</span></a></li>

            <li class="{{$uriPage == 'minha-rede' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/minha-rede')}}"><i class='fa fa-users'></i> <span>Rede Binária</span></a></li>
            <li class="{{$uriPage == 'vouchers' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/vouchers')}}"><i class='fa fa-gg'></i> <span>Meus Vouchers</span></a></li>
            <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/materiais')}}"><i class='fa fa-folder'></i> <span>Materiais</span></a></li>
            <li ><a href="mailto:contato@sevenbrasil.co"><i class='fa fa-support'></i> <span>Suporte</span></a></li>

            @endif

            @if (! Auth::guest() && Auth::user()->isAdmin() && $uriPainel == 'admin')
            <li class="{{$uriPage == 'vouchers' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/vouchers')}}"><i class='fa fa-users'></i> <span>Gerenciar Vouchers</span></a></li>
            <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/materiais')}}"><i class='fa fa-folder'></i> <span>Materiais</span></a></li>
            @endif
        </ul><!-- /.sidebar-menu -->
        @endif
    </section>
    <!-- /.sidebar -->
</aside>
