<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <strong><a href="{{env('SITE_URL', 'http://seven')}}">Seven - Marketing & Network</a>.</strong> <!-- Created by <a href="http://mpdev.com.br">Miller P.</a> -->
    </div>
    <!-- Default to the left -->
   &nbsp;
</footer>