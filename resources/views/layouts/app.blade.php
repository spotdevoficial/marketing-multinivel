<!DOCTYPE html>
<html lang="en">
@include('layouts.partials.htmlheader')
<body class="skin-black sidebar-mini">
<div class="wrapper f">
    @include('layouts.partials.mainheader')
    @include('layouts.partials.sidebar')
            <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('layouts.partials.contentheader')
                <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->
            @yield('main-content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->
    @include('layouts.partials.controlsidebar')
    @include('layouts.partials.footer')
</div><!-- ./wrapper -->
@include('layouts.partials.scripts')
</body>
</html>