@extends('layouts.app')

@section('htmlheader_title')
Transações
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Transações
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Transações</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="display:none;">Id</th>
                            <th>Usuario</th>
                            <th>Descrição</th>
                            <th>Data</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('extratos', 'App\extratos')
                        @foreach($extratos->where('beneficiado', Auth::user()->id)->get() as $extrato)
                        <tr>
                            <td style="display:none;">{{$extrato['id']}}</td>
                            <td>@if($extrato['descricao']=='Pagamento de binário')
                                {{'SevenBrasil'}}
                                @endif
                                @if($extrato['descricao']!='Pagamento de binário')
                                {{$extrato->userName($extrato['user_id'])}}
                                @endif


                            </td>
                            <td>{{$extrato['descricao']}}</td>

                            <td>{{$extrato['data']}}</td>
                            <td>R${{number_format($extrato['valor'],2)}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
@endsection