@extends('layouts.app')

@section('htmlheader_title')
Dashboard
@endsection

@section('contentheader_title')

@endsection

@section('breadcrumb')
@endsection

@section('contentheader_description')

@endsection

@section('main-content')

<!-- Main row -->
<div class="row">



    <section class="col-lg-12">

        <div class="panel panel-info text-center">
            <div class="panel-heading">
                Efetuar Pagamento
            </div>
            <div class="panel-body">
                <a id="efetuarPagamento" target="_blank" class="btn btn-lg btn-primary">Clique aqui para efetuar o Pagamento</a>
            </div>
        </div>

    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

@endsection

@section('page_scripts')
<!-- iCheck 1.0.1 -->
<script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/form/jquery.form.min.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/iCheck/icheck.min.js') }}"></script>
<script type='text/javascript'>

function fBlockUi() {
    $.blockUI({
        message: "<h4>Por favor aguarde...</h4>",
        css: {
            border: 'none',
            padding: '5px',
            backgroundColor: '#000',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .5,
            color: '#fff'
        }
    });
}

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass: 'iradio_minimal-blue'
});
$("#efetuarPagamento").click(function () {
    $(this).html('Por favor aguarde...');
    
    $.ajax({
        'url': "?novopagamento=1",
        dataType: 'html',
        'success': function (txt) {
            
            $('#efetuarPagamento').removeClass('btn-primary');
            $('#efetuarPagamento').addClass('btn-success')
            $('#efetuarPagamento').html('Redirecionando...');
            setTimeout(function(){ location.href=txt; }, 2000);

        }
    });
});
</script>


@endsection