@extends('layouts.app')

@section('htmlheader_title')
    Meus Dados
@endsection

@section('contentheader_title')
    Meus Dados
@endsection

@section('contentheader_description')

@endsection


@section('main-content')

    <div class="row">

        <section class="col-lg-6">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Dados Pessoais</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Dados Bancários</a></li>
                </ul>

                <form role="form" method="post" action="">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="box-body">
                        <div class="tab-content">
                                <div class="tab-pane active" id="tab_1">

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" required placeholder="Nome Completo" name="name" value="{{old('name') ? old('name') : Auth::user()->name}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="email" class="form-control" readonly required placeholder="Email" name="email" value="{{ old('email') ? old('email') : Auth::user()->email}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label>Direção da Rede:</label>
                                        <select name="direcao" class="form-control">
                                            <option {{ (old('direcao') == 'esquerda' OR Auth::user()->direcao == 'esquerda') ? 'selected' : '' }} value="esquerda">
                                                Esquerda
                                            </option>
                                            <option {{ (old('direcao') == 'direita' OR Auth::user()->direcao == 'direita') ? 'selected' : '' }} value="direita">
                                                Direita
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="password" class="form-control" placeholder="Senha" name="password"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="password" class="form-control" placeholder="Repita a senha" name="password_confirmation"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <label>Sexo:</label>
                                        <select name="sexo" class="form-control">
                                            <option {{ (old('sexo') == 'Masculino' OR Auth::user()->sexo == 'Masculino') ? 'selected' : '' }} value="Masculino">
                                                Masculino
                                            </option>
                                            <option {{ (old('sexo') == 'Feminino' OR Auth::user()->sexo == 'Feminino') ? 'selected' : '' }} value="Feminino">
                                                Feminino
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control cpf" required placeholder="CPF" name="cpf" value="{{old('cpf') ? old('cpf') : Auth::user()->cpf}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control data" placeholder="Data de Nascimento" name="nascimento" value="{{old('nascimento') ? old('nascimento') : Auth::user()->getNascimento()}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control telefone" data-inputmask="'mask': ['99-9999-9999[9]', '+99 99 9999-9999[9]']" data-mask placeholder="Telefone" name="telefone" value="{{old('telefone') ? old('telefone') : Auth::user()->telefone}}"/>
                                    </div>


                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Endereço" name="endereco" value="{{old('endereco') ? old('endereco') : Auth::user()->endereco}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Bairro" name="bairro" value="{{old('bairro') ? old('bairro') : Auth::user()->bairro}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Cidade" name="cidade" value="{{old('cidade') ? old('cidade') : Auth::user()->cidade}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Estado" name="estado" value="{{old('estado') ? old('estado') : Auth::user()->estado}}"/>
                                    </div>

                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_2">
                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Banco" name="banco" value="{{old('banco') ? old('banco') : Auth::user()->banco}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Agência" name="agencia" value="{{old('agencia') ? old('agencia') : Auth::user()->agencia}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Conta" name="conta" value="{{old('conta') ? old('conta') : Auth::user()->conta}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Tipo de Conta" name="tipo_conta" value="{{old('tipo_conta') ? old('tipo_conta') : Auth::user()->tipo_conta}}"/>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <input type="text" class="form-control" placeholder="Operação" name="operacao" value="{{old('operacao') ? old('operacao') : Auth::user()->operacao}}"/>
                                    </div>
                                </div>

                                <!-- /.tab-pane -->
                            </div>
                            <!-- /.tab-content -->
                        </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Atualizar</button>
                    </div>
                </form>
            </div><!-- /.box -->

        </section>

    </div>

    <!-- Main row -->
    <div class="row">

        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">


        </section><!-- /.Left col -->


    </div><!-- /.row (main row) -->

    @endsection

    @section('page_scripts')
            <!-- InputMask -->
    <script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
    <script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
    <script>
        $(function () {
            $('.data').inputmask("99-99-9999");
            $('.cpf').inputmask("999.999.999-99");
            $("[data-mask]").inputmask();
        });
    </script>
@endsection
