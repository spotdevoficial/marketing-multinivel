@extends('layouts.app')

@section('htmlheader_title')
Adicionar Produto
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Adicionar Produto
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">
<div class="box"><div class="box-body">
   <div class="register-box" style="width: 850px; margin-top:0px;
   margin-left:0px">
  
            <form action="{{ url('/painel/produtos/add') }}" enctype="multipart/form-data" method="POST">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="col-sm-9">

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control"  placeholder="Nome" maxlength="20" name="nome" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control"  placeholder="Preco" name="preco" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control "  placeholder="Peso" name="peso" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                         <input type="file" class="form-control "  name="img" value=""/> 
                    </div>



                    <div class="form-group has-feedback">
                        <input type="text" class="form-control "  placeholder="Categoria" name="categoria" value=""/>
                    </div>

                <div class="row">
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
                    </div><!-- /.col -->
                </div>

            </form>
        </div><!-- /.form-box --></div><!-- /.form-box -->
    </div><!-- /.register-box -->


</div>

@endsection

