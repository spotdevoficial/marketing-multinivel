@extends('layouts.app')

@section('htmlheader_title')
    Dashboard
@endsection

@section('contentheader_title')
    Dashboard
@endsection

@section('contentheader_description')
    Painel do Usuario
    @endsection

    @section('main-content')
            <!-- Small boxes (Stat box) -->
    <div class="row">

    </div><!-- /.row -->

    <div class="row">

        <section class="col-lg-6">

            <div class="box box-warning">
                <div class="box-header with-border text-center">
                    <h3 class="box-title ">Link de indicação</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body text-center">
                    <h4><a href="{{url('/cadastro/'.Auth::user()->email)}}" data-toggle="tooltip" title="Use a URL abaixo para indicar um amigo e ganhe R$ 40,00 em saldo." target="_blank">{{url('/cadastro/'.Auth::user()->email)}}</a></h4>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </section>

    </div>

    <!-- Main row -->
    <div class="row">

        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">


        </section><!-- /.Left col -->


    </div><!-- /.row (main row) -->

@endsection