@extends('layouts.app')

@section('htmlheader_title')
Pedido número {{$pedido['id']}}
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Loja Virtual > Meus pedidos > Pedido número {{$pedido['id']}}
@endsection

@section('contentheader_description')

@endsection

@section('main-content')

<div class="container-fluid">

    <div class="box">
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">

                    <div id="mensagemAdicionarVouchers">

                    </div>

                    <div class="col-md-5">
                        <h4>Endereco de entrega</h4>
                        <div class="row">
                            <div class="box box-info">
                                <div class="box-body">
                                    <p>{{$endereco['pais']}}</p>
                                    <p>{{$endereco['endereco']}}</p>
                                    <p>CEP {{$endereco['cep']}} - {{$endereco['cidade']}}, {{$endereco['estado']}}</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-offset-1 col-md-6">

                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    ID do pedido
                                </td>
                                <td>
                                    {{$pedido['id']}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Status
                                </td>
                                <td>
                                    {{$pedido['status']}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Preco + frete
                                </td>
                                <td>
                                    R$ {{number_format($pedido['preco'],2)}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Data
                                </td>
                                <td>
                                    {{Carbon\Carbon::parse($pedido['date'])->format('d/m/Y')}}
                                </td>
                            </tr>
                        </table>

                    </div>

                    <!-- INICIO FORMULARIO BOTAO PAGSEGURO -->
                    <form action="https://pagseguro.uol.com.br/checkout/v2/payment.html" method="post">
                    <!-- NÃO EDITE OS COMANDOS DAS LINHAS ABAIXO -->
                    <input type="hidden" name="code" value="{{$pedido['id_pag']}}" />
                    <input type="image" src="https://stc.pagseguro.uol.com.br/public/img/botoes/pagamentos/209x48-comprar-assina.gif" name="submit" alt="Pague com PagSeguro - é rápido, grátis e seguro!" />
                    </form>
                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                    <table id="ord-carrinho" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> </th>
                                <th>Produto</th>
                                <th>Preço</th>
                                <th>Quantidade</th>
                                <th>Total</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($produtos as $produto)
                            <tr>
                                <td style="
                                    background-image: url('/public/produtos/{{$produto['produto']['img']}}');
                                    background-position: center;
                                    background-size: cover;
                                    height: 77px;">
                                </td>
                                <td>{{$produto['produto']['nome']}}</td>
                                <td>R$ {{number_format($produto['produto']['preco'],2)}}</td>
                                <td>{{$produto['quantidade']}}</td>
                                <td>R$ {{number_format($produto['produto']['preco']*$produto['quantidade'],2)}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>



                </div>
            </div>
        </div>


    </div>

</div>

@endsection

@section('page_scripts')
@endsection
