@extends('layouts.app')

@section('page_css')
    <link rel="stylesheet" href="{{env('CFURL').('/css/tree.css')}}">
    <style>
        .img-responsive {
            margin: 0 auto;
            width: 50px;
            margin-bottom: 5px;
        }

        .popover {
            min-width: 300px;
            max-width: 500px;
        }

        .popover .popover-content {
            padding: 10px;
        }

        .popover .popover-title {
            padding: 10px;
        }

        .tree li a {
            color: #000;
        }

        @media screen and (max-width: 1024px) {
            .arvore {

            }
        }

        @media screen and (min-width: 1250px) {
            .arvore {
                margin: 0 auto;
            }
        }

        .tree li a {
            border: none;
            padding: 0;
            margin: 0;
        }

        .tree li a:hover, .tree li a:hover + ul li a {
            background: none;
            border: none;
        }

        /*Connector styles on hover*/
        .tree li a:hover + ul li::after,
        .tree li a:hover + ul li::before,
        .tree li a:hover + ul::before,
        .tree li a:hover + ul ul::before {
            border-color: #6C7A89;
        }

        .tree .init:before {
            border: none;
        }

    </style>
@endsection

@section('htmlheader_title')
    Minha Rede
@endsection

@section('contentheader_title')
    Minha Rede
@endsection

@section('breadcrumb')
    <li class="active">Minha Rede</li>
    @endsection

    @section('contentheader_description')

    @endsection

    @section('main-content')

    <?php
    function printPopOver($id, $login, $nome, $status)
    {
        $status = $status ? 'Ativo' : 'Inativo';
        if (Auth::user()->isAdmin()) {
            $indicador = "Indicador: <b>" . Auth::user()->getIndicador($id) . "</b><br/>";
        } else {
            $indicador = '';
        }
        $url_rede = url('/painel/minha-rede/'.$id);
        $html = <<<EOL
            ID:<b>$id</b><br/>
            Login:<b>$login</b><br/>
            $indicador
            Nome:<b>$nome</b></br>
            Status: </b>$status</b></br>
            <a class="btn btn-primary" href="$url_rede">Visualizar Rede</a>
EOL;


        if (Auth::user()->isAdmin()) {

            $csrf = csrf_field();
            $url = url('painel/minha-rede/update/' . $id);

            $options = <<<OPT
            <option value="esquerda">Esquerda</option>
            <option value="direita">Direita</option>
OPT;

            $posicao = Auth::user()->getUserDirection($id);
            if ($posicao == 'esquerda') {
                $options = <<<OPT
            <option value="esquerda" selected>Esquerda</option>
            <option value="direita">Direita</option>
OPT;
            } elseif ($posicao == 'direita') {
                $options = <<<OPT
            <option value="esquerda">Esquerda</option>
            <option value="direita" selected>Direita</option>
OPT;
            }


            $html .= <<<EOL
            <form class="formAjax" method="POST" action="$url" style="z-index: 1000;">
            $csrf
            <select name="direcao" class="form-control updatePositionDirecao">
            $options
            </select>
            </br>
            </br>
            </form>
EOL;
        }
        return $html;
    }
    ?>
            <!-- Small boxes (Stat box) -->
    <div class="row" id="tree-family">

        <div class="col-md-12">
            <div id="mensagemAjax">

            </div>
            <div class="" style="">

                <div class="col-lg-2 col-xs-3">
                    <!-- small box -->
                    <div class="small-box bg-gray">

                        <div class="inner">
                            <h3>{{$user_interna->totalEsquerda()}}</h3>
                            <p>Total Esquerda</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-network"></i>
                        </div>
                    </div>
                </div><!-- ./col -->

                @if(Auth::user()->isAdmin())
                    <div class="col-lg-2 col-xs-3">

                        <div class="small-box bg-gray">
                            <div class="inner">
                                <p> Buscar Usuario</p>
                                <form class="formAjax form-horizontal" action="{{url('painel/minha-rede/busca')}}" method="POST">
                                    {{csrf_field()}}
                                    <label>
                                        <input name="busca" class="form-control" placeholder="Login ou ID">
                                    </label>
                                    <button type="submit" class="btn btn-primary">Buscar</button>
                                </form>
                            </div>

                            <div class="icon">
                                <i class="ion ion-search"></i>
                            </div>

                            <small> &nbsp;</small>
                        </div>

                    </div>
                @endif

                <div class="col-lg-2 col-xs-3 pull-right">
                    <!-- small box -->
                    <div class="small-box bg-gray">

                        <div class="inner">
                            <h3>{{$user_interna->totalDireita()}}</h3>
                            <p>Total Direita</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-network"></i>
                        </div>

                    </div>
                </div><!-- ./col -->

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="">
                        <div class="tree container">
                            <div class="">
                                <ul class="arvore">
                                    <li>
                                        <ul class="init">
                                            <?php $loop = 0; ?>
                                            <?php $loop1 = 0; ?>
                                            <?php $loop2 = 0; ?>
                                            <?php $loop3 = 0; ?>
                                            <li>
                                                <div href="{{url('/painel/minha-rede/'.($user_interna->id))}}">
                                                    <div id="level-0" data-toggle="popover" data-content="{{printPopOver($user_interna->id,$user_interna->username,$user_interna->name,$user_interna->ativo)}}">
                                                        <img src="{{($user_interna->ativado() ? url('/img/user_ativo.png') : url('/img/user_inativo.png'))}}" class="img-responsive">

                                                    </div>
                                                </div>

                                                <ul>
                                                    <?php
                                                    $lado = 1;
                                                    ?>
                                                    @if($user_interna->getFilhos())
                                                        @foreach($user_interna->getFilhos() as $filho)
                                                            <?php $loop++; ?>
                                                            <li class="{{$lado == 1 ? 'direito' : 'esquerdo'}}">
                                                                <div href="{{url('/painel/minha-rede/'.($filho->id))}}">
                                                                    <div id="level-1" data-toggle="popover" data-content="{{printPopOver($filho->id,$filho->username,$filho->name,$filho->ativo)}}">
                                                                        <img src="{{($filho->ativado() ? url('/img/user_ativo.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                $lado = 2;
                                                                ?>

                                                                <ul>
                                                                    @if($filho->getFilhos())
                                                                        @foreach($filho->getFilhos() as $filho1)
                                                                            <?php $loop1++; ?>
                                                                            <li>
                                                                                <div href="{{url('/painel/minha-rede/'.($filho1->id))}}">
                                                                                    <div id="level-2" data-toggle="popover" data-content="{{printPopOver($filho1->id,$filho1->username,$filho1->name,$filho1->ativo)}}">
                                                                                        <img src="{{($filho1->ativado() ? url('/img/user_ativo.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                    </div>
                                                                                </div>
                                                                                <ul>
                                                                                    @if($filho1->getFilhos())
                                                                                        @foreach($filho1->getFilhos() as $filho2)
                                                                                            <?php $loop2++; ?>
                                                                                            <li>
                                                                                                <div href="{{url('/painel/minha-rede/'.($filho2->id))}}">
                                                                                                    <div id="level-3" data-toggle="popover" data-content="{{printPopOver($filho2->id,$filho2->username,$filho2->name,$filho2->ativo)}}">
                                                                                                        <img src="{{($filho2->ativado() ? url('/img/user_ativo.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <ul>
                                                                                                    @if($filho2->getFilhos())
                                                                                                        @foreach($filho2->getFilhos() as $filho3)
                                                                                                            <?php $loop3++; ?>
                                                                                                            <li>
                                                                                                                <div href="{{url('/painel/minha-rede/'.($filho3->id))}}">
                                                                                                                    <div id="level-4" data-toggle="popover" data-content="{{printPopOver($filho3->id,$filho3->username,$filho3->name,$filho3->ativo)}}">
                                                                                                                        <img src="{{($filho3->ativado() ? url('/img/user_ativo.png') : url('/img/user_inativo.png'))}}" class="img-responsive">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </li>
                                                                                                        @endforeach
                                                                                                        @if($loop3 == 1)
                                                                                                            <li>
                                                                                                                <div href="#">
                                                                                                                    <div>
                                                                                                                        <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </li>
                                                                                                        @endif
                                                                                                        <?php $loop3 = 0; ?>
                                                                                                    @else
                                                                                                        @for($id = 0; $id < 2; $id++)
                                                                                                            <li>
                                                                                                                <div href="#">
                                                                                                                    <div>
                                                                                                                        <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </li>
                                                                                                        @endfor
                                                                                                    @endif
                                                                                                </ul>
                                                                                            </li>
                                                                                        @endforeach
                                                                                        @if($loop2 == 1)
                                                                                            <li>
                                                                                                <div href="#">
                                                                                                    <div>
                                                                                                        <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <ul>
                                                                                                    @for($l3 = 0; $l3 < 2; $l3++)
                                                                                                        <li>
                                                                                                            <div href="#">
                                                                                                                <div>
                                                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    @endfor
                                                                                                </ul>
                                                                                            </li>
                                                                                        @endif
                                                                                        <?php $loop2 = 0; ?>
                                                                                    @else
                                                                                        @for($a = 0; $a < 2; $a++)
                                                                                            <li>
                                                                                                <div href="#">
                                                                                                    <div>
                                                                                                        <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                    </div>
                                                                                                </div>
                                                                                                <ul>
                                                                                                    @for($aa1 = 0; $aa1 < 2; $aa1++)
                                                                                                        <li>
                                                                                                            <div href="#">
                                                                                                                <div>
                                                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                    @endfor
                                                                                                </ul>
                                                                                            </li>
                                                                                        @endfor
                                                                                    @endif
                                                                                </ul>
                                                                            </li>
                                                                        @endforeach
                                                                        @if($loop1 == 1)
                                                                            <li>
                                                                                <div href="#">
                                                                                    <div>
                                                                                        <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                    </div>
                                                                                </div>
                                                                                <ul>
                                                                                    @for($h = 0; $h < 2; $h++)
                                                                                        <li>
                                                                                            <div href="#">
                                                                                                <div>
                                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                </div>
                                                                                            </div>
                                                                                            <ul>
                                                                                                @for($h1 = 0; $h1 < 2; $h1++)
                                                                                                    <li>
                                                                                                        <div href="#">
                                                                                                            <div>
                                                                                                                <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                @endfor
                                                                                            </ul>
                                                                                        </li>
                                                                                    @endfor
                                                                                </ul>
                                                                            </li>
                                                                        @endif
                                                                        <?php $loop1 = 0; ?>
                                                                    @else
                                                                        @for($b = 0; $b < 2; $b++)
                                                                            <li>
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                                <ul>
                                                                                    @for($c = 0; $c < 2; $c++)
                                                                                        <li>
                                                                                            <div href="#">
                                                                                                <div>
                                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                </div>
                                                                                            </div>
                                                                                            <ul>
                                                                                                @for($ce = 0; $ce < 2; $ce++)
                                                                                                    <li>
                                                                                                        <div href="#">
                                                                                                            <div>
                                                                                                                <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                @endfor
                                                                                            </ul>
                                                                                        </li>
                                                                                    @endfor
                                                                                </ul>
                                                                            </li>
                                                                        @endfor
                                                                    @endif
                                                                </ul>
                                                            </li>
                                                        @endforeach
                                                        @if($loop == 1)

                                                            <li>
                                                                <div href="#">
                                                                    <div>
                                                                        <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                    </div>
                                                                </div>
                                                                <ul>
                                                                    @for($g = 0; $g < 2; $g++)
                                                                        <li>
                                                                            <div href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </div>
                                                                            <ul>
                                                                                @for($h = 0; $h < 2; $h++)
                                                                                    <li>
                                                                                        <div href="#">
                                                                                            <div>
                                                                                                <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                            </div>
                                                                                        </div>
                                                                                        <ul>
                                                                                            @for($k = 0; $k < 2; $k++)
                                                                                                <li>
                                                                                                    <div href="#">
                                                                                                        <div>
                                                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </li>
                                                                                            @endfor
                                                                                        </ul>
                                                                                    </li>
                                                                                @endfor
                                                                            </ul>
                                                                        </li>
                                                                    @endfor
                                                                </ul>
                                                            </li>
                                                        @endif
                                                        <?php $loop = 0; ?>
                                                    @else
                                                        @for($d = 0; $d < 2; $d++)
                                                            <li>
                                                                <div href="#">
                                                                    <div>
                                                                        <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                    </div>
                                                                </div>
                                                                <ul>
                                                                    @for($e = 0; $e < 2; $e++)
                                                                        <li>
                                                                            <div href="#">
                                                                                <div>
                                                                                    <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                </div>
                                                                            </div>
                                                                            <ul>
                                                                                @for($f = 0; $f < 2; $f++)
                                                                                    <li>
                                                                                        <div href="#">
                                                                                            <div>
                                                                                                <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                            </div>
                                                                                        </div>
                                                                                        <ul>
                                                                                            @for($j = 0; $j < 2; $j++)
                                                                                                <li>
                                                                                                    <div href="#">
                                                                                                        <div>
                                                                                                            <img src="{{url('/img/user_vazio.png')}}" class="img-responsive">
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </li>
                                                                                            @endfor
                                                                                        </ul>
                                                                                    </li>
                                                                                @endfor
                                                                            </ul>
                                                                        </li>
                                                                    @endfor
                                                                </ul>
                                                            </li>
                                                        @endfor
                                                    @endif
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>


    <!-- Main row -->
    <div class="row">

        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">

        </section><!-- /.Left col -->


    </div><!-- /.row (main row) -->

@endsection
@section('page_scripts')
    <script src="{{ asset('/plugins/form/jquery.form.min.js') }}"></script>
    <script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>
    <script src="{{env('CFURL').('/plugins/zoomooz/jquery.zoomooz.min.js')}}"></script>
    <script>

        $(document).ready(function () {
            var lengths = $('.direito').map(function () {
                return $(this).find('li').length;
            }).get();


        });

        $(function () {

            function fBlockUi() {
                $.blockUI({
                    message: "<h4>Por favor aguarde...</h4>",
                    css: {
                        border: 'none',
                        padding: '5px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '5px',
                        '-moz-border-radius': '5px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
            }

            $('.formAjax').ajaxForm({
                // target identifies the element(s) to update with the server response
                target: '#mensagemAjax',
                beforeSubmit: function () {
                    fBlockUi();
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAjax').clearForm();
                    $('#mensagemAjax').fadeIn('slow');
                    $.unblockUI();
                }
            });

            $('[data-toggle="popover"]').popover({
                html: true,
                trigger: 'manual',
                container: $(this).attr('id'),
                placement: 'top',
                content: function () {
                    $return = '<div class="hover-hovercard"></div>';
                }
            }).on("mouseenter", function () {
                var _this = this;
                $(this).popover("show");
                $(this).siblings(".popover").on("mouseleave", function () {
                    $(_this).popover('hide');
                });
                $('.updatePositionDirecao').change(function () {
                    $(this).parent('form').submit();
                });


                $('.formAjax').ajaxForm({
                    // target identifies the element(s) to update with the server response
                    target: '#mensagemAjax',
                    // success identifies the function to invoke when the server response
                    // has been received; here we apply a fade-in effect to the new content
                    success: function () {
                        $('.formAjax').clearForm();
                        $('#mensagemAjax').fadeIn('slow');
                    }
                });
            }).on("mouseleave", function () {
                var _this = this;
                setTimeout(function () {
                    if (!$(".popover:hover").length) {
                        $(_this).popover("hide")
                    }
                }, 100);
            });
        })

    </script>
@endsection