@extends('layouts.app')

@section('htmlheader_title')
Upgrade
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Upgrade
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Upgrade</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="container">
                    <h2>Pacote Atual </h2>
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#pct">{{$pacoteAtual['nome']}}</a>
                                </h4>
                            </div>
                            <div id="pct" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li class="list-group-item">{{$pacoteAtual['descricao']}}</li>
                                    <li class="list-group-item">{{$pacoteAtual['produtos']}}</li>
                                    <li class="list-group-item">R${{$pacoteAtual['valor']}}</li>
                                </ul>

                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="container">
                    <h2>Pacotes disponiveis </h2>
                    <div class="panel-group">
                        <?php $i = 0; ?>
                        @foreach($pacotes as $pacote)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse{{$i}}">{{$pacote['nome']}}</a>
                                </h4>
                            </div>
                            <div id="collapse{{$i}}" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <li class="list-group-item">{{$pacote['descricao']}}</li>
                                    <li class="list-group-item">{{$pacote['produtos']}}</li>
                                    <li class="list-group-item">De R${{$pacote['valor']}} por apenas R${{$pacote['valor']-$desconto}}</li>
                                </ul>
                                <a  onclick="upgrade({{$pacote['id']}}, this)"  style="color: #fff; cursor: pointer;">
                                    <div class="panel-footer  bg-blue-gradient">
                                        <span id="euQuero">Eu quero este pacote!</span> <i class="fa fa-arrow-circle-right"></i>

                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php $i++; ?>

                        @endforeach
                    </div>
                </div>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
                                            $(function () {

                                            $('#example2').DataTable({
                                            "paging": true,
                                                    "lengthChange": true,
                                                    "searching": true,
                                                    "ordering": true,
                                                    "info": true,
                                                    "autoWidth": true
                                            });
                                            });
                                            function upgrade(id, ele){
                                            $(ele).find('#euQuero').html('Por favor aguarde...');
                                                    $(ele).find('.panel-footer').removeClass('bg-blue-gradient');
                                                    $(ele).find('.panel-footer').addClass('bg-orange');
                                                    $.ajax({
                                                    'url': "?novoUpgrade=1&id="+id,
                                                            dataType: 'html',
                                                            'success': function (txt) {
                                                            $(ele).find('.panel-footer').removeClass('bg-orange');
                                                                    $(ele).find('.panel-footer').addClass('bg-green');
                                                                    $(ele).find('#euQuero').html('Redirecionando...');
                                                                    setTimeout(function(){ location.href=txt; }, 2000);

                                                            }
                                                    });
                                            }
</script>
@endsection