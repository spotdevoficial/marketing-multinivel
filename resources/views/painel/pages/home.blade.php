<?php $__env->startSection('htmlheader_title'); ?>
Dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_title'); ?>
Dashboard
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page_css'); ?>
<style>

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentheader_description'); ?>
Painel do Usuario
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<style>
.box.box-warning {
    border-top-color: #fff;
}
</style>
<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-dollar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Seu Saldo</span>
                <span class="info-box-number">R$ <?php echo e(Auth::user()->getSaldo()); ?></span>
            </div>
        </div>
    </div> 
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-network"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Total na Rede</span>
                <span class="info-box-number"><?php echo e(Auth::user()->display_children()); ?></span>
            </div>
        </div>
    </div> 
    <div class="col-md-3 col-sm-6 col-xs-12">

        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-person-add"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><a href="<?php echo e(url('painel/meus-indicados')); ?>" class="small-box-footer" style="color:#333;">Indicados Diretos</a></span>
                <span class="info-box-number"><?php echo e(Auth::user()->nIndicados() > 0 ? Auth::user()->nIndicados() : 0); ?></span>
            </div>


        </div>
    </div> 



    <div class="col-md-3 col-sm-6 col-xs-12">

        <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-pie-graph"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Visitantes</span>
                <span class="info-box-number"><?php echo e(Auth::user()->visitas()); ?></span>
            </div>


        </div>
    </div> 
</div><!-- /.row -->

<div class="row">
    <section class="col-lg-6">

        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title ">Link de indicação</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <h4><a href="<?php echo e(url('/cadastro/' . Auth::user()->username)); ?>" target="_blank"><?php echo e(url('/cadastro/' . Auth::user()->username)); ?></a></h4>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section>
    <section class="col-lg-6">

        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title ">Lado Binário</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <div class="form-group">

                    <div class="radio-inline">
                        <label>
                            <input type="radio" class="change_dir"  name="ladobinario" id="ladobinario" value="direita">
                            Direito
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" class="change_dir" name="ladobinario" id="ladobinario2" value="esquerda">
                            Esquerdo
                        </label>
                    </div>

                </div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section>
    <section class="col-lg-6">

        <div class="box box-warning">
            <div class="box-header with-border text-center">
                <h3 class="box-title ">Dados</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body text-center">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Plano</th>
                            <th>Graduação</th>
                            <th>Binário</th>
                            <th>Qualificado</th>
                            <th>Porcentagem</th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$pacoteCurr['nome']}}</td>
                            <td>Micro-franqueado</td>
                            <td><?php
                                if ($pacoteCurr['binario'] == 1) {
                                    echo '<span class="label label-success">Sim</span>';
                                } else {
                                    echo '<span class="label label-danger">Não</span>';
                                }
                                ?></td>
                            <td><?php
                                $indicados = Auth::user()->nIndicadosDir();
                                if ($indicados['esquerda'] > 0 and $indicados['direita'] > 0) {
                                    echo '<span class="label label-success">Sim</span>';
                                } else {
                                    echo '<span class="label label-danger">Não</span>';
                                }
                                ?></td>
                            <td>{{100*$pacoteCurr['porcentagem']}}%</td>   

                        </tr>
                    </tbody>



                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </section>
    <section class="col-lg-6">
        <div class="info-box bg-aqua">
            <span class="info-box-icon">D</span>
            <div class="info-box-content">
                <span class="info-box-text">Direita</span>
                <span class="info-box-number">{{$binario['direita']}} PB</span>
                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                    {{Auth::user()->totalDireita()}} cadastro(s)
                </span>
            </div><!-- /.info-box-content -->
        </div>
        <div class="info-box bg-red">
            <span class="info-box-icon">E</span>
            <div class="info-box-content">
                <span class="info-box-text">Esquerda</span>
                <span class="info-box-number">{{$binario['esquerda']}} PB</span>
                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                    {{Auth::user()->totalEsquerda()}} cadastro(s)
                </span>
            </div><!-- /.info-box-content -->
        </div>
    </section>
</div>

<!-- Main row -->
<div class="row">

    <!-- Left col -->
    <section class="col-lg-6 connectedSortable">


    </section><!-- /.Left col -->


</div><!-- /.row (main row) -->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('page_scripts'); ?>
<!-- InputMask -->
<script src="<?php echo e(env('CFURL') . ('/plugins/form/jquery.form.min.js')); ?>"></script>

<script>
$(document).ready(function () {
// bind form using ajaxForm
    $('#formSuporteDashboard').ajaxForm({
        // target identifies the element(s) to update with the server response
        target: '#mensagemSuporteDashboard',
        beforeSubmit: function () {
            $('#sendEmail').attr('disabled', true);
            Pace.restart();
        },
        // success identifies the function to invoke when the server response
        // has been received; here we apply a fade-in effect to the new content
        success: function () {
            $('#formSuporteDashboard').clearForm();
            $('#mensagemSuporteDashboard').fadeIn('slow');
            setTimeout(function () {
                Pace.restart();
                location.reload();
            }, 1500);
        }
    });
});
$(document).ready(function () {
    var ladoBin = "<?php echo e(Auth::user()->direcao); ?>";
    if (ladoBin == 'direita') {
        $("#ladobinario").attr('checked', true);
    } else {
        $("#ladobinario2").attr('checked', true);

    }

});
$(".change_dir").click(function () {
    var lado = $(this).val();
    $.ajax({
        'url': "muda_lado?lado=" + lado,
        dataType: 'html',
        'success': function (txt) {
            if (txt == 'ok') {
                alert('O lado binário foi alterado com sucesso.');
            }
            if (txt == 'fail') {
                alert('O lado não binário foi alterado com sucesso.');

            }
        }});
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>