@extends('layouts.app')

@section('htmlheader_title')
Transações
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Loja Virtual > Produtos disponíveis
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="container-fluid">

    <div class="row">

        <div id="mensagemAdicionarVouchers">

        </div>

        <form method="get" action="{{url('/painel/produtos/')}}">
            <!-- Input de pesquisa -->
            <div class="col-md-12" style="
                        margin-bottom: 10px;
                    ">
                <div class="input-group"
                    style="
                        border-radius: 0px;
                        height: 30px;
                    ">

                    <span class="input-group-addon" id="basic-addon1"
                        style="
                            background-color: #222D32;
                            color: white;
                            border-style: none;
                        ">
                        <i class="fa fa-search"></i>
                    </span>
                    <input type="text" value="{{$busca}}" name="busca" class="form-control" placeholder="Por qual tipo de produto deseja buscar?"
                        style="
                        height: 50px;
                        color: #B8C7CE;
                        font-size: 20px;
                        background-color: #222D32;
                        border-style: none;
                        ">
                </div>
            </div>
        </form>
        <!-- Fim do Input de pesquisa -->

        <!-- Informação da busca -->
        @if(!empty($busca))
            <div class="container-fluid"
                style="
                    margin: 20px 0px;
                ">
                <h1>Busca por "{{$busca}}"</h1>
            </div>
        @endif
        <!-- Fim da Informação da busca -->

        <!-- Tiles de produtos -->
        @foreach($produtos as $produto)

              <div class="col-md-3">
                  <div class="box box-solid text-center"
                      style="
                          border-style: solid;
                          border-radius: 0px;
                          border-width: 1px;
                          border-color: #C5C5C5;
                      ">
                      <div class="box-header with-border"
                        style="
                          background-image: url('{{ url($produto['img']) }}');
                          background-position: center;
                          background-size: cover;
                          height: 200px;">

                      </div>
                      <div class="box-body">
                          <h4>{{$produto['nome']}}</h4>
                          <h5>{{$produto['categoria']}}</h5>
                          <h4>R$ {{number_format($produto['preco'],2)}}</h4>
                          <a href="/painel/meu-carrinho/add/{{$produto['id']}}"><button type="button" class="btn btn-flat btn-responsive btn-block" name="button">Adicionar</button></a>
                      </div>
                  </div>
              </div>

        @endforeach

        @if(sizeof($produtos) == 0)

          <h1 class="text-center"
              style="
              margin-top: 170px;
              color: #D6D6D6;
              font-size: 100px;
              font-weight: bold;
              ">
              <i class="fa fa-shopping-cart"></i>
          </h1>
          <h1 class="text-center"
              style="
              color: #D6D6D6;
              font-size: 50px;
              font-weight: bold;
              ">
              Sem produtos disponíveis.
          </h1>

        @endif

        <!-- Fim das tiles de produtos -->
    </div>

</div>

@endsection

@section('page_scripts')
@endsection
