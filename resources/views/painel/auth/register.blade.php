@extends('layouts.auth')

@section('htmlheader_title')
    Cadastro
@endsection

@section('content')

    <body class="hold-transition register-page">
    <div class="register-box" style="width: 650px">
        <div class="register-logo">
            <a href="{{ url('/') }}">
                <img src="{{url('/img/logo-login.png')}}">
            </a>
        </div>

        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="register-box-body">
            <p class="login-box-msg">Você foi indicado por: <b>{{$indicador->name. ' - '. $indicador->email}}</b></p>
            <form action="{{ url('/cadastro/'.$indicador->username) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="col-sm-6">

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required placeholder="Usuario" maxlength="20" name="username" value="{{old('username')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required placeholder="Nome Completo" name="name" value="{{old('name')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control cpf" required placeholder="CPF" name="cpf" value="{{old('cpf')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" required placeholder="Email" name="email" value="{{old('email')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" required placeholder="Senha" name="password" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" required placeholder="Repita a senha" name="password_confirmation" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <select name="sexo" class="form-control">
                            <option value="Masculino" selected="">
                                Masculino
                            </option>
                            <option value="Feminino">
                                Feminino
                            </option>
                      
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">


                    <div class="form-group has-feedback">
                        <input type="text" class="form-control data" placeholder="Data de Nascimento" name="nascimento" value="27-07-1996"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control telefone" data-inputmask="'mask': ['99-9999-9999[9]', '+99 99 9999-9999[9]']" data-mask placeholder="Telefone" name="telefone" value="{{old('telefone')}}"/>
                    </div>


                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Endereço" name="endereco" value="{{old('endereco')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Bairro" name="bairro" value="{{old('bairro')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Cidade" name="cidade" value="{{old('cidade')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Estado" maxlength="2" name="estado" value="{{old('estado')}}"/>
                    </div>
                     <div class="form-group has-feedback">
                        <select name="pacote" class="form-control">

                        @foreach($pacotes as $pacote)
                           @if ($pacote->status==1)
                            <option value="{{$pacote->id}}">
                                {{$pacote->nome}}-R${{$pacote->valor}}
                            </option>
                            @endif
                        @endforeach
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
                    </div><!-- /.col -->
                </div>

            </form>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    @include('layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });

            $('.data').inputmask("dd-mm-yyyy");
            $('.cpf').inputmask("999.999.999-99");
            $("[data-mask]").inputmask();
        });
    </script>
    .<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-72220910-1', 'auto');
        ga('send', 'pageview');

    </script>
    </body>

@endsection
