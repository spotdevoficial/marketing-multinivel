<form class="formAjax" method="post" action="{{url('/admin/pacote')}}">


    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Pacote</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="nome"/>
        </div>
        <div class="form-group">
            <label>Descrição </label>
            <textarea class="form-control" required placeholder="Descrição" name="descricao">
                
            </textarea>
        </div>
        <div class="form-group">
            <label>Produtos </label>
            <textarea class="form-control" required placeholder="Produtos" name="produtos">
                
            </textarea>
        </div>
        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div
        <div class="form-group">
            <label>Valor</label>
            <input type="text" class="form-control" required placeholder="Valor" name="valor"/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>